import configparser
import telebot
from twilio.rest import Client
from phone import Phone

config = configparser.ConfigParser()
config.read('config.ini')

account_sid = config["twilio"]["account_sid"]
auth_token = config["twilio"]["auth_token"]
telegram_token = config["telegram"]["token"]
bot = telebot.TeleBot(telegram_token)

phone = Phone(account_sid, auth_token)

client = Client(account_sid, auth_token)

user_number = ""
send_to = ""


@bot.message_handler(commands=['start', 'help'])
def start(message):
    print(message)
    bot.reply_to(message, "Howdy, how are you doing?")


@bot.message_handler(commands=['usenumber'])
def use_number(message):
    global user_number
    user_number = message.text.replace("/usenumber ", "", 1)
    bot.reply_to(message, f"Using number: {user_number}")


@bot.message_handler(commands=['sendto'])
def set_send(message):
    global send_to
    send_to = message.text.replace("/sendto ", "", 1)
    bot.reply_to(message, f"Will send to number: {send_to}")


@bot.message_handler(commands=['message'])
def set_user_number(message):
    try:
        body = message.text.replace("/message ", "", 1)
        result = phone.send_message(user_number, send_to, body)
        print(result)
        bot.reply_to(message, "done")
    except Exception as e:
        bot.reply_to(message, str(e))


@bot.message_handler(commands=['list'])
def list_available(message):
    area_code = message.text.replace("/list ", "", 1)
    numbers = phone.list_available(area_code)
    numbers_text = '\n'.join(numbers)
    bot.reply_to(message, f"Available Phone Numbers: \n{numbers_text}")


@bot.message_handler(commands=['mynumbers'])
def my_numbers(message):
    numbers = phone.my_numbers()
    numbers_text = '\n'.join(numbers)
    bot.reply_to(message, f"Your numbers are: \n{numbers_text}")


@bot.message_handler(commands=['mymessages'])
def my_numbers(message):
    messages = phone.get_messages()
    messages_text = '\n'.join(messages)
    bot.reply_to(message, f"Your messages are: \n{messages_text}")


@bot.message_handler(commands=['buynumber'])
def buy_number(message):
    number = message.text.replace("/buynumber ", "", 1)
    save_number = phone.buy_number(number)
    bot.reply_to(message, f"Your new number is: {save_number}")


@bot.message_handler(func=lambda m: True)
def echo_all(message):
    print(message)
    bot.reply_to(message, message.text)


bot.polling()
