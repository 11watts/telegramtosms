from twilio.rest import Client


class Phone:
    def __init__(self, account_sid, auth_token):
        # print(auth_token)
        # self.account_sid = account_sid
        # self.auth_token = auth_token
        self.client = Client(account_sid, auth_token)
        # print(self.account_sid)

    def my_numbers(self):
        numbers = self.client.incoming_phone_numbers.list(limit=20)
        return list(map(lambda n: n.friendly_name, numbers))

    def list_available(self, area_code):
        numbers = self.client.available_phone_numbers('US').local.list(
            area_code,
            limit=10
        )
        return list(map(lambda n: n.friendly_name, numbers))

    def buy_number(self, phone_number):
        try:
            print(phone_number)
            incoming_phone_number = self.client.incoming_phone_numbers.create(
                phone_number=phone_number
            )
            return incoming_phone_number.friendly_name
        except Exception as e:
            return str(e)

    def send_message(self, from_, to, body):
        message = self.client.messages.create(
            body=body,
            from_=from_,
            to=to
        )
        return message

    def get_messages(self):
        messages = self.client.messages.list(limit=10)
        return list(map(lambda m: f"from '{m.from_}\n to: {m.to} \nbody: {m.body}", messages))
