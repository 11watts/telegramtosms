import configparser
import unittest
from phone import Phone
from twilio.base.exceptions import TwilioRestException, TwilioException

config = configparser.ConfigParser()
config.read('../config.ini')

account_sid = config["twilio"]["account_sid"]
auth_token = config["twilio"]["auth_token"]
test_account_sid = config["twilio"]["test_account_sid"]
test_auth_token = config["twilio"]["test_auth_token"]

testApiPhone = Phone(test_account_sid, test_auth_token)
realApiPhone = Phone(account_sid, auth_token)


class PhoneTest(unittest.TestCase):
    def test_list_available(self):
        numbers = realApiPhone.list_available(210)
        self.assertTrue(all(n.startswith("(210)") for n in numbers))

    def test_buy_number(self):
        error = testApiPhone.buy_number("+15005550000")
        self.assertEqual(error, "HTTP 400 error: Unable to create record: +15005550000 is not available")
        # self.assertRaises(TwilioException, testApiPhone.buy_number, "+15005550000")
        valid_number = "+15005550006"
        incoming_phone_number = testApiPhone.buy_number("+15005550006")
        # print(incoming_phone_number)
        self.assertEqual(incoming_phone_number, "(500) 555-0006")

    def test_send(self):
        result = testApiPhone.send_message("+15005550006", "+15005550006", "body")
        self.assertEqual(result.status, "queued")

if __name__ == '__main__':
    unittest.main()
